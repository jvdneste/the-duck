import 'package:flutter/material.dart';

class CustomFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      height: 50,
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [
              0.1,
              0.3,
              0.7,
              0.9,
            ],
            colors: [
              Colors.red[900].withOpacity(0.5),
              Colors.black12,
              Colors.black12,
              Colors.red[900].withOpacity(0.5),
            ]),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          'Copyright © 2020 The Duck Team. All rights reserved.',
        ),
      ),
    );
  }
}
