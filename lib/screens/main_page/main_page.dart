import 'package:flutter/material.dart';

import 'components/header.dart';

import '../home_page/home_page.dart';
import '../about_us_page/about_us_page.dart';
import '../support_page/support_page.dart';
import '../add_page/add_page.dart';

class MainPage extends StatefulWidget {
  static const String id = 'HomePage';

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _currentIndex = 0;

  Widget _setScreen(int index) {
    switch (index) {
      case 0:
        return HomePage();
        break;
      case 1:
        return AddPage();
        break;
      case 2:
        return SupportPage();
        break;
      case 3:
        return AboutUsPage();
        break;
      default:
        return HomePage();
        break;
    }
  }

  void _onButtonTap(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red,
      child: Scaffold(
        appBar: Header(
          homeButtonCallback: () => _onButtonTap(0),
          addButtonCallback: () => _onButtonTap(1),
          supportButtonCallback: () => _onButtonTap(2),
          aboutUsButtonCallback: () => _onButtonTap(3),
        ),
        body: _setScreen(_currentIndex),
      ),
    );
  }
}
