import 'package:flutter/material.dart';

class HeaderActionButton extends StatelessWidget {
  HeaderActionButton({
    this.text,
    @required this.onPressed,
    this.color,
    this.hoverColor,
    this.borderSideColor,
  });

  final String text;
  final Function onPressed;
  final Color color;
  final Color hoverColor;
  final Color borderSideColor;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text(
        text,
        style: TextStyle(color: Colors.white60),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
        side: BorderSide(color: borderSideColor),
      ),
      hoverColor: hoverColor,
      onPressed: onPressed,
      color: color,
    );
  }
}
