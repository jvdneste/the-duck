import 'package:flutter/material.dart';
import 'header_actions.dart';

class Header extends StatefulWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize; // default value is 56.0

  final Function homeButtonCallback;
  final Function addButtonCallback;
  final Function supportButtonCallback;
  final Function aboutUsButtonCallback;

  Header(
      {Key key,
      @required this.homeButtonCallback,
      @required this.addButtonCallback,
      @required this.supportButtonCallback,
      @required this.aboutUsButtonCallback})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.black87,
      centerTitle: false,
      title: InkWell(
        focusColor: Colors.black87,
        highlightColor: Colors.black87,
        splashColor: Colors.black87,
        hoverColor: Colors.black87,
        onTap: widget.homeButtonCallback,
        child: Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                height: 40,
                width: 80,
                child: Image(
                  image: AssetImage('assets/ducklogo.png'),
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'The Duck Team',
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white54,
                ),
              )
            ],
          ),
        ),
      ),
      actions: [
        HeaderActions(
          addButtonCallback: widget.addButtonCallback,
          aboutUsButtonCallback: widget.aboutUsButtonCallback,
          supportButtonCallback: widget.supportButtonCallback,
        ),
      ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => throw UnimplementedError();
}
