// ignore: avoid_web_libraries_in_flutter
import 'dart:html';

import 'package:flutter/material.dart';
import 'package:theduckproject/screens/support_page/components/support_page_text_field.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../components/custom_footer.dart';

class SupportPage extends StatefulWidget {
  @override
  _SupportPageState createState() => _SupportPageState();
}

class _SupportPageState extends State<SupportPage> {
  List<String> attachments = [];
  bool isHTML = false;

  final _recipientController = TextEditingController(
    text: 'lyubomir2030@gmail.com',
  );

  final _subjectController = TextEditingController();

  final _bodyController = TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  sendMessage() async {
    String platformResponse;

    print('1');
//    final MailOptions mailOptions = MailOptions(
//      body: '$_bodyController',
//      subject: '$_subjectController',
//      recipients: ['$_recipientController'],
//      isHTML: isHTML,
//      //bccRecipients: ['$_recipientController'],
//      //ccRecipients: ['$_recipientController'],
//      attachments: [
//        '$attachments',
//      ],
//    );
    final String _email = 'mailto:' +
        _recipientController.text +
        '?subject=' +
        _subjectController.text +
        '&body=' +
        _bodyController.text;
    try {
      await launch(_email);
      print('2');

      platformResponse = 'success';
    } catch (e) {
      platformResponse = e.toString();
    }
    print('3');

//    try {
//      await FlutterMailer.send(mailOptions);
//    print('2');
//
//    platformResponse = 'success';
//  } catch (e) {
//  platformResponse = e.toString();
//  }

    if (!mounted) return;

    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(platformResponse),
    ));

    print('maybe sent');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10.0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.black26,
                    borderRadius: BorderRadius.circular(20),
                    gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      stops: [
                        0.0,
                        0.2,
                        0.4,
                        0.6,
                        0.8,
                        1,
                      ],
                      colors: [
                        Colors.black12,
                        Colors.black26,
                        Colors.black38,
                        Colors.black38,
                        Colors.black26,
                        Colors.black12,
                      ],
                    )),
                height: 70,
                width: MediaQuery.of(context).size.width / 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      minRadius: 20,
                      maxRadius: 30,
                      backgroundImage: AssetImage('ducklogo.png'),
                      backgroundColor: Colors.blueAccent,
                    ),
                    SizedBox(
                      width: 25,
                    ),
                    Text(
                      'He are happy to help You!',
                      style: TextStyle(
                          color: Colors.deepOrange.withOpacity(0.7),
                          fontSize: 22,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    MyCustomTextField(
                      padding: EdgeInsets.only(bottom: 10),
                      height: 50,
                      controller: _recipientController,
                      maxLines: 1,
                      labelText: 'Recipient',
                      //hintText: 'Don`t do anything here',
                    ),
                    MyCustomTextField(
                      padding: EdgeInsets.all(10),
                      height: 50,
                      controller: _subjectController,
                      maxLines: 1,
                      labelText: 'Subject',
                      hintText: 'Text your problem overall...',
                    ),
                    MyCustomTextField(
                      padding: EdgeInsets.all(10),
                      height: 200,
                      controller: _bodyController,
                      maxLines: 6,
                      labelText: 'Describe your problem more clearly',
                    ),
                    ...attachments.map(
                      (item) => Text(
                        item,
                        overflow: TextOverflow.visible,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
//                          FloatingActionButton.extended(
//                            icon: Icon(Icons.attach_file),
//                            label: Text(
//                              'Add file',
//                              style: TextStyle(
//                                color: Colors.white,
//                              ),
//                            ),
//                            backgroundColor: Colors.black12,
//                            hoverColor: Colors.deepOrange.withOpacity(0.5),
//                            onPressed: null, //_filePicker,
//                          ),
//                          SizedBox(
//                            width: 100,
//                          ),
                          FloatingActionButton.extended(
                              icon: Icon(Icons.send),
                              label: Text(
                                'Send',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                              backgroundColor: Colors.black12,
                              hoverColor: Colors.deepOrange.withOpacity(0.5),
                              onPressed: () {
                                setState(() {
                                  sendMessage();
                                });
                              }),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
            CustomFooter(),
          ],
        ),
      ),
    );
  }

//  void _filePicker() async {
//    File _files = await FilePicker.getFile();
//    setState(() {
//      attachments.add(_files.toString());
//      print('${_files.toString()} hy');
//      print('${_files.name} hi');
//    });
//  }
}
