import 'package:flutter/material.dart';

class MyCustomTextField extends StatelessWidget {
  MyCustomTextField({
    this.controller,
    this.height,
    this.labelText,
    this.padding,
    this.maxLines,
    this.hintText,
  });

  final TextEditingController controller;
  final double height;
  final int maxLines;
  final String labelText;
  final EdgeInsetsGeometry padding;
  final String hintText;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Container(
        width: MediaQuery.of(context).size.width / 3,
        height: height,
        child: TextField(
          controller: controller,
          maxLines: maxLines,
          decoration: InputDecoration(
            labelText: labelText,
            hintText: hintText,
            border: OutlineInputBorder(),
          ),
        ),
      ),
    );
  }
}
