import 'package:flutter/material.dart';
import 'package:getflutter/getflutter.dart';

class BlockDescription extends StatelessWidget {
  BlockDescription({
    this.imageOverlay,
    this.imageAvatar,
    this.name,
    this.description,
    this.content,
  });

  final String content;
  final String imageOverlay;
  final String imageAvatar;
  final String name;
  final String description;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 230,
      width: 380,
      child: GFCard(
        //colorFilter: ColorFilter.mode(Colors.black, BlendMode.overlay),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        semanticContainer: true,
        imageOverlay: AssetImage(
          imageOverlay,
        ),
        boxFit: BoxFit.cover,
        title: GFListTile(
          avatar: GFAvatar(
            radius: 40,
            backgroundImage: AssetImage(imageAvatar),
          ),
          title: Text(
            name,
            style: TextStyle(fontSize: 25, color: Colors.greenAccent),
          ),
          subTitle: Text(
            description,
          ),
        ),
        content: Text(
          content,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 14,
          ),
        ),
      ),
    );
  }
}
