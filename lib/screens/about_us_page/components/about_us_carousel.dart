import 'package:flutter/material.dart';
import 'package:getflutter/getflutter.dart';

class MyOwnCarousel extends StatelessWidget {
  MyOwnCarousel({this.onPageChange});

  final Function onPageChange;

  final List<String> textCarousel = [
    'Haven`t heard about us?',
    'Want to know more about the developers?',
    'For that and more look below!',
  ];

  @override
  Widget build(BuildContext context) {
    return GFCarousel(
      height: 100,
      viewportFraction: 1,
      autoPlay: true,
      autoPlayInterval: Duration(seconds: 4),
      items: textCarousel.map((text) {
        return Container(
          margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [
                0.1,
                0.5,
                1,
              ],
              colors: [
                Colors.black26.withOpacity(0.3),
                Colors.blueGrey[700],
                Colors.black26.withOpacity(0.3),
              ],
            ),
          ),
          constraints: BoxConstraints(
            minHeight: 20,
            maxHeight: 60,
            minWidth: 300,
            maxWidth: 700,
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Center(
              child: Text(
                text,
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
        );
      }).toList(),
      onPageChanged: onPageChange,
    );
  }
}
