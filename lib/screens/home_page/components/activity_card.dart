import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ActivityCard extends StatelessWidget {
  final Uint8List imageBytes;
  final String name;
  final String description;
  final String url;

  ActivityCard(
      {@required this.imageBytes,
      @required this.name,
      @required this.description,
      @required this.url});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        color: Colors.grey[700],
        height: 200,
        width: 400,
        child: Row(
          children: [
            Expanded(
              child: Image(
                image: MemoryImage(imageBytes),
                width: 100,
                height: 200,
                fit: BoxFit.fill,
              ),
            ),
            Expanded(
              child: ListView(
                children: [
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: Text(
                      name,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.greenAccent,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: Text(
                      description,
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  url != null
                      ? RaisedButton(
                          child: Text('GoTo'),
                          onPressed: () async {
                            if (await canLaunch(url)) {
                              await launch(
                                url,
                                forceSafariVC: false,
                                forceWebView: false,
                              );
                            } else {
                              print('can\'t launch $url');
                            }
                          },
                        )
                      : SizedBox(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
