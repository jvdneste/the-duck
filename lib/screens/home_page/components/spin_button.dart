import 'package:flutter/material.dart';

class SpinButton extends StatefulWidget {
  final Function onPressed;

  SpinButton({@required this.onPressed});

  @override
  _SpinButtonState createState() => _SpinButtonState();
}

class _SpinButtonState extends State<SpinButton>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 3),
    );
    _animation = Tween(
      begin: 0.0,
      end: 100.0,
    ).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Curves.linear,
      ),
    );
  }

  startOrStop() {
    if (_animationController.isAnimating) {
      _animationController.stop();
    } else {
      _animationController.reset();
      _animationController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
        startOrStop();
      },
      child: AnimatedBuilder(
        animation: _animation,
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(shape: BoxShape.circle),
          child: Image(
            fit: BoxFit.contain,
            image: AssetImage('assets/spin1.png'),
          ),
        ),
        builder: (context, child) {
          return Transform.rotate(
            angle: _animation.value,
            child: child,
          );
        },
      ),
    );
  }
}
