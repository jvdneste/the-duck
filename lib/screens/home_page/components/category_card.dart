import 'package:flutter/material.dart';

class CategoryCard extends StatelessWidget {
  final String category;

  CategoryCard({@required this.category});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        height: 100,
        width: 200,
        decoration: BoxDecoration(
          color: Colors.grey[700],
        ),
        child: Center(
          child: Text(
            category,
            style: TextStyle(
              fontSize: 25,
              color: Colors.greenAccent,
            ),
          ),
        ),
      ),
    );
  }
}
