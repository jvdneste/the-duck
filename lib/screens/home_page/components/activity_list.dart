import 'dart:convert';

import 'package:flutter/material.dart';

import '../../../services/spin_service.dart';
import 'activity_card.dart';

class ActivityList extends StatefulWidget {
  final String category;
  final List initialActivities;

  ActivityList({this.category, this.initialActivities});

  @override
  _ActivityListState createState() => _ActivityListState();
}

class _ActivityListState extends State<ActivityList> {
  List<ActivityCard> activityCards = [];

  void _addActivities(List newActivities) {
    for (final activity in newActivities) {
      final bytesString = activity['img'].toString().substring(23);
      final imageBytes = base64Decode(bytesString);

      final String name = activity['name'];
      String description = activity['description'];

      final urlPattern =
          r'(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})';
      final linkRegExp = RegExp('$urlPattern', caseSensitive: false);
      final match = linkRegExp.firstMatch(description);
      String url;

      if (match != null) {
        url = description.substring(match.start);
        print(url);
        description =
            description.replaceRange(match.start, description.length, '');
      }

      activityCards.add(ActivityCard(
        name: name,
        description: description,
        imageBytes: imageBytes,
        url: url,
      ));
    }
  }

  void _loadMore() async {
    try {
      final newActivities = await SpinService.loadActivities(widget.category);
      setState(() {
        _addActivities(newActivities);
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void didUpdateWidget(ActivityList oldWidget) {
    activityCards.clear();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    if (activityCards.isEmpty) {
      _addActivities(widget.initialActivities);
    }

    return ListView.builder(
      itemCount: activityCards.length + 1,
      itemBuilder: (_, index) {
        return index == activityCards.length
            ? FlatButton(
                child: Text('Load more'),
                onPressed: _loadMore,
              )
            : activityCards[index];
      },
      physics: BouncingScrollPhysics(),
    );
  }
}
