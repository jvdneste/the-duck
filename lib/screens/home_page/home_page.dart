import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../.../../../services/spin_service.dart';
import '../../components/custom_footer.dart';
import 'components/activity_list.dart';
import 'components/category_card.dart';
import 'components/spin_button.dart';
import 'components/spinner_text_carousel.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List _activities = [];
  String _category = '';

  void _spin() async {
    try {
      final Map<String, dynamic> initialActivities =
          await SpinService.spinActivities();
      setState(() {
        _category = initialActivities['category'];
        _activities = initialActivities['activities'];
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: _activities.isNotEmpty
                ? MainAxisAlignment.spaceEvenly
                : MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    width: 450,
                    height: 100,
                    child: TextCarousel(),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SpinButton(
                    onPressed: _spin,
                  ),
                ],
              ),
              _category.isNotEmpty
                  ? CategoryCard(
                      category: _category,
                    )
                  : SizedBox(),
              _activities.isNotEmpty
                  ? Container(
                      width: 400,
                      height: 700,
                      child: ActivityList(
                        category: _category,
                        initialActivities: _activities,
                      ),
                    )
                  : SizedBox(),
            ],
          ),
          CustomFooter(),
        ],
      ),
    );
  }
}
