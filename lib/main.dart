import 'package:flutter/material.dart';

import 'screens/main_page/main_page.dart';

void main() {
  runApp(TheDuckApp());
}

class TheDuckApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark().copyWith(
        highlightColor: Colors.deepOrange.withOpacity(0.5),
        errorColor: Colors.red,
        accentColor: Colors.deepOrange.withOpacity(0.5),
      ),
      home: MainPage(),
    );
  }
}
