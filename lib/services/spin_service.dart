import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class SpinService {
  static final String _apiUrl = 'http://localhost:8080/';
  static final String _spinUrl = '$_apiUrl/spin';
  static final String _spinMoreActivitiesUrl = '$_apiUrl/activities';

  static Future<Map<String, dynamic>> spinActivities() async {
    final response =
        await http.get(_spinUrl, headers: {'Content-Type': 'application/json'});
    if (response.statusCode == 200) {
      final result = jsonDecode(utf8.decode(response.bodyBytes));
      if (result == null) {
        throw 'Body is null';
      } else if (result['activities'] == []) {
        throw 'There is no activities in this category';
      } else if (result != null) {
        return result;
      }
    } else {
      throw response.statusCode;
    }
  }

  static Future loadActivities(String category) async {
    final response = await http.get('$_spinMoreActivitiesUrl/$category',
        headers: {'Content-Type': 'application/json'});
    if (response.statusCode == 200) {
      final decodedData = jsonDecode(utf8.decode(response.bodyBytes));
      return decodedData['activities'];
    } else {
      throw response.statusCode;
    }
  }
}
